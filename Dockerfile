# syntax=docker/dockerfile:1.3

FROM opensuse/leap:15.4

LABEL org.opencontainers.image.source="https://git.psi.ch/viessm_h/hackk8s" \
      org.opencontainers.image.title="Hackathon K8s Test Image" \
      org.opencontainers.image.description="" \
      org.opencontainers.image.authors=""

# gracefully kill the container
STOPSIGNAL SIGRTMIN+3

# notify systemd that we are in a container
ENV container docker

# set hostname (sanity check)
ENV HOSTNAME ifyoucanreadthissomethingwentwrong

RUN set -ex \
    && zypper --gpg-auto-import-keys refresh -s \
    && zypper install -l -y \
       tar \
       gawk \
       wget \
       bzip2 \
       python3 python3-pip \
       openssh-server openssh-clients \
       iproute2 \
       hostname \
       file \
       bash-completion \
       vim \
       nano \
       sudo \
       systemd-coredump \
    && zypper clean --all

# Add ttyd
ADD https://github.com/tsl0922/ttyd/releases/download/1.7.2/ttyd.x86_64 /usr/bin/ttyd
COPY ./ttyd.service /etc/systemd/system/ttyd.service 
COPY ./ttyd-sysuser.conf /etc/sysusers.d/ttyd.conf
RUN set -ex \
    && chmod +x /usr/bin/ttyd

# Podman related issue (SUSE container default?), see https://lists.podman.io/archives/list/podman@lists.podman.io/thread/H2I5SNMHI3TANGA7RWX4KZKMIKC3YQLW/
RUN mkdir -p /etc/systemd/system/systemd-logind.service.d/ \
    && echo -e "[Service]\nProtectHostname=no" > /etc/systemd/system/systemd-logind.service.d/10-disable-protecthostname.conf

# SUSE per-default requires root password for all SUDO operations
# We disable this here
RUN sed -i -e '/^ALL/ s/./#&/' \
           -e '/^Defaults targetpw/ s/./#&/' \
           -e '/wheel.*NOPASSWD/ s/^# *//' /etc/sudoers

# No need for graphical.target here
RUN set -ex \
    && systemctl set-default multi-user.target \
    && systemctl enable ttyd \
    && systemctl enable sshd

CMD ["/usr/lib/systemd/systemd", "--system"]
